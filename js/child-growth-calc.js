const childGrowthHeight = $('#child-growth-calc-height-input');
const childGrowthWeight = $('#child-growth-calc-weight-input');
const childGrowthAge = $('#child-growth-calc-age-input');
const childGrowthGenderSelector = $('#child-growth-calc-gender-selector');

const childGrowthResultHeightHeader = $('#child-growth-result-height-header');
const childGrowthResultWeightHeader = $('#child-growth-result-weight-header');

const childGrowthResultHeightBody = $('#child-growth-result-height-body');
const childGrowthResultWeightBody = $('#child-growth-result-weight-body');

const childGrowthHeightProgressContainer = $('#child-growth-result-height-progress-container');
const childGrowthWeightProgressContainer = $('#child-growth-result-weight-progress-container');

const childGrowthHeightScaleContainer = $('#child-growth-result-height-scale-container');
const childGrowthWeightScaleContainer = $('#child-growth-result-weight-scale-container');

const childGrowthHeightRangeContainer = $('#child-growth-result-height-scale-range-container');
const childGrowthWeightRangeContainer = $('#child-growth-result-weight-scale-range-container');

const childGrowthSubmitButton = $('#btn_Submit_child-growthCalc_Right');
const childGrowthResultButton = $('#child-growth-result-return-button');


const data = {
    m: {},
    f: {}
};

const childGrowthResponses = [
    [
        'قيمة منخفضة',
        '%5 من الأطفال ذوي قيمة أقل'
    ],
    [
        'قيمة متوسطة منخفضة',
        'ما بين %5-%50 من الأطفال ذوي قيمة أقل'
    ],
    [
        'قيمة متوسطة مرتفعة',
        'ما بين %50-%95 من الأطفال ذوي قيمة أقل'
    ],
    [
        'قيمة مرتفعة',
        '%95 من الأطفال ذوي قيمة أقل'
    ]
];

const childGrowthErrors = {
    age: 'Please enter a valid age (between 2 and 20)',
    height: 'Please enter a valid height (between 25 and 250cm)',
    weight: 'Please enter a valid weight (between 1 and 150cm)',
    gender: 'Please select a valid gender'
};

const childGrowthHeightRange = [70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200];
const childGrowthWeightRange = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

const childGrowthGetData = async () => {
    const files = [
        'm_h',
        'm_w',
        'f_h',
        'f_w',
    ];

    for(let file in files) {
        const path = `./data/${files[file]}_data.json`;
        const response = await fetch(path);

        switch(+file) {
            case 0: data.m.h = await response.json(); break;
            case 1: data.m.w = await response.json(); break;
            case 2: data.f.h = await response.json(); break;
            case 3: data.f.w = await response.json(); break;
        };
    }
};

const childGrowthGetResults = (age, height, weight, gender) => {
    try {
        childGrowthHideErrors();
        const heightPercentile = childGrowthGetHeightPercentile(age, height, gender);
        const weightPercentile = getWeightPercentile(age, weight, gender);
    
        return [heightPercentile, weightPercentile];
    } catch(error) {
        childGrowthShowError(error);
    }
};

const childGrowthGetHeightPercentile = (age, height, gender) => {
    if(!childGrowthValidateAge(age)) {
        throw {
            type: 'age',
            message: childGrowthErrors.age
        };
    }

    if(!childGrowthValidateHeight(height)) {
        throw {
            type: 'height',
            message: childGrowthErrors.height
        };
    }

    if(!childGrowthValidateGender(gender)) {
        throw {
            type: 'gender',
            message: childGrowthErrors.gender
        };
    }

    age = childGrowthConvertYearsToMonths(age);

    const p5 = data[gender].h[age].p5;
    const p50 = data[gender].h[age].p50;
    const p95 = data[gender].h[age].p95;

    const p5Progress = getChildGrowthProgress(p5, childGrowthHeightRange[0], childGrowthHeightRange[childGrowthHeightRange.length - 1])
    const p50Progress = getChildGrowthProgress(p50, childGrowthHeightRange[0], childGrowthHeightRange[childGrowthHeightRange.length - 1]);
    const p95Progress = getChildGrowthProgress(p95, childGrowthHeightRange[0], childGrowthHeightRange[childGrowthHeightRange.length - 1]);

    const progress = getChildGrowthProgress(height, childGrowthHeightRange[0], childGrowthHeightRange[childGrowthHeightRange.length - 1], 'h');

    if(height < p5) return [childGrowthResponses[0], [p5Progress, p50Progress, p95Progress], progress];
    else if(height >= p5 && height < p50) return [childGrowthResponses[1], [p5Progress, p50Progress, p95Progress], progress];
    else if(height >= p50 && height < p95) return [childGrowthResponses[2], [p5Progress, p50Progress, p95Progress], progress];
    else return [childGrowthResponses[3], [p5Progress, p50Progress, p95Progress], progress];
};

const getWeightPercentile = (age, weight, gender) => {
    if(!childGrowthValidateAge(age)) {
        throw {
            type: 'age',
            message: childGrowthErrors.age
        };
    }

    if(!childGrowthValidateWeight(weight)) {
        throw {
            type: 'weight',
            message: childGrowthErrors.weight
        };
    }

    if(!childGrowthValidateGender(gender)) {
        throw {
            type: 'gender',
            message: childGrowthErrors.gender
        };
    }

    age = childGrowthConvertYearsToMonths(age);

    const p5 = data[gender].w[age].p5;
    const p50 = data[gender].w[age].p50;
    const p95 = data[gender].w[age].p95;

    const p5Progress = getChildGrowthProgress(p5, childGrowthWeightRange[0], childGrowthWeightRange[childGrowthWeightRange.length - 1], 'w');
    const p50Progress = getChildGrowthProgress(p50, childGrowthWeightRange[0], childGrowthWeightRange[childGrowthWeightRange.length - 1], 'w');
    const p95Progress = getChildGrowthProgress(p95, childGrowthWeightRange[0], childGrowthWeightRange[childGrowthWeightRange.length - 1], 'w');

    const progress = getChildGrowthProgress(weight, childGrowthWeightRange[0], childGrowthWeightRange[childGrowthWeightRange.length - 1], 'w');

    if(weight < p5) return [childGrowthResponses[0], [p5Progress, p50Progress, p95Progress], progress];
    else if(weight >= p5 && weight < p50) return [childGrowthResponses[1], [p5Progress, p50Progress, p95Progress], progress];
    else if(weight >= p50 && weight < p95) return [childGrowthResponses[2], [p5Progress, p50Progress, p95Progress], progress];
    else return [childGrowthResponses[3], [p5Progress, p50Progress, p95Progress], progress];
};

const getChildGrowthProgress = (p, l, h, metric = 'h') => {
    // FORMULA:
    // (if metric = h then round(percentile) else percentile - lowest value) / (highest value - lowest value) * 100

    return (((metric === 'h' ? Math.round(p) : p) - l) / (h - l) * 100).toFixed(1);
};

const childGrowthConvertYearsToMonths = years => {
    return years * 12;
};

const childGrowthValidateAge = age => {
    return age && age !== '' && !isNaN(age) && age >= 2 && age <= 20;
};

const childGrowthValidateHeight = height => {
    return height && height !== '' && !isNaN(height) && height >= 25 && height <= 250;
};

const childGrowthValidateWeight = weight => {
    return weight && weight !== '' && !isNaN(weight) && weight >= 1 && weight <= 150;
};

const childGrowthValidateGender = gender => {
    return gender && gender !== '' && (gender === 'm' || gender === 'f');
};

const childGrowthCalcInit = () => {
    childGrowthGetData();
};

childGrowthSubmitButton.click(() => {
    const [heightPercentile, weightPercentile] = childGrowthGetResults(childGrowthAge.val(), childGrowthHeight.val(), childGrowthWeight.val(), childGrowthGenderSelector.val());

    childGrowthResultHeightHeader.html(heightPercentile[0][0]);
    childGrowthResultWeightHeader.html(weightPercentile[0][0]);

    childGrowthResultHeightBody.html(heightPercentile[0][1]);
    childGrowthResultWeightBody.html(weightPercentile[0][1]);

    childGrowthHeightRangeContainer.empty();
    for(let n of childGrowthHeightRange) {
        childGrowthHeightRangeContainer.append(`<li>${n}</li>`);
    }

    childGrowthWeightRangeContainer.empty();
    for(let n of childGrowthWeightRange) {
        childGrowthWeightRangeContainer.append(`<li>${n}</li>`);
    }

    childGrowthHeightScaleContainer.empty();
    for(let p in heightPercentile[1]) {
        childGrowthHeightScaleContainer.append(`<div class='p-circle p-${p}' style='right: ${heightPercentile[1][p]}%;'></div>`)
    }

    childGrowthWeightScaleContainer.empty();
    for(let p in weightPercentile[1]) {
        childGrowthWeightScaleContainer.append(`<div class='p-circle p-${p}' style='right: ${weightPercentile[1][p]}%;'></div>`)
    }

    childGrowthHeightProgressContainer.empty();
    childGrowthHeightProgressContainer.append(`<div class='child-growth-progress-bar' style='width: ${heightPercentile[2]}%;'></div>`);

    childGrowthWeightProgressContainer.empty();
    childGrowthWeightProgressContainer.append(`<div class='child-growth-progress-bar' style='width: ${weightPercentile[2]}%;'></div>`);

    childGrowthShowResults();
});

childGrowthResultButton.click(() => {
    childGrowthHideResults();
});

const childGrowthHideResults = () => {
    $('.child-growth-result').css('display', 'none');
    $('.calcWzDescCnts').css('display', 'block');
};

const childGrowthShowResults = () => {
    $('.child-growth-result').css('display', 'flex');
    $('.calcWzDescCnts').css('display', 'none');
};

const childGrowthHideErrors = () => {
    const errorContainer = $(`.child-growth-error-container`);
    errorContainer.html('');
};

const childGrowthShowError = error => {
    const errorContainer = $(`#child-growth-${error.type}-error`);
    errorContainer.html(error.message);
};